import 'package:conversor_curso/app_controller.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  State<HomePage> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<HomePage> {
  int counter = 0;

  @override
  Widget build(BuildContext context) {
    // um widget com varias funçoes facilitadoreas para criar a tela
    return Scaffold(
      //DRAWER MENU LATERAL
      drawer: Drawer(
        child: Column(
          children: [
            UserAccountsDrawerHeader(
                //CLIPRRECT DEIXA A FOTO REDONDA, ELE CORTA OS SEUS FLILHOS
                currentAccountPicture: ClipRRect(
                  borderRadius: BorderRadius.circular(40),
                  child: Image.asset('assets/images/user.jpg'),
                ),
                accountName: Text('Gustavo Gabriel'),
                accountEmail: Text('gustavo@gmail.com')),
            //CRIA UMA OPÇÃP COM ESSES COMPONENTES
            ListTile(
              leading: Icon(Icons.home),
              title: Text('Home'),
              subtitle: Text('Tela Inicial'),
              //AO CLICAR REDIRECIONA
              onTap: () {
                Navigator.of(context).pushReplacementNamed('/home');
              },
            ),
            ListTile(
              leading: Icon(Icons.logout),
              title: Text('Logout'),
              subtitle: Text('Finalizas sessão'),
              onTap: () {
                Navigator.of(context).pushReplacementNamed('/');
              },
            ),
            ListTile(
              leading: Icon(Icons.star),
              title: Text('Desafio'),
              subtitle: Text('Desafio Flutterando'),
              onTap: () {
                Navigator.of(context).pushReplacementNamed('/desafio');
              },
            )
          ],
        ),
      ),
      appBar: AppBar(
        title: Center(
          child: Text('APP GUSTAVO FLUTTER', style: TextStyle(fontSize: 15)),
        ),
        actions: [CustomSwitcher()],
      ), // o header o cabeçalho da tela

      // corpo da tela
      body: Container(
        //alarga o tamanho da coluna para a tela toda
        /* width: double.infinity,
        height: double.infinity, */

        //Row sentido horizontal, Column sentido vertical
        //Usa se ListView quando precisa scrola a tela, muiutos elementos
        child: ListView(
            //centraliza no centro da coluna sentido vertical

            //mainAxisAlignment: MainAxisAlignment.spaceAround,

            /*define a posição e o tamanho dos elementos dentro da column
          crossAxisAlignment: CrossAxisAlignment.start,*/
            //da uma possibilidade de scrol horizontal como se fosse filmes netfliz
            //scrollDirection: Axis.horizontal,
            children: [
              //esse container cria um espaço entre o elemento abaixo e o acima
              SpaceVertical(size: 50),
              Row(
                children: [
                  Text('Texto dentro da row'),
                ],
              ),

              //concatenação com variavel
              Text('Contador: $counter'),
              SpaceVertical(size: 15),
              CustomSwitcher(),
              SpaceVertical(size: 50),
              Squares(),
              Squares(),
              Squares(),

              //usando a componentização do Squares
            ]),
      ),

      // configura um botão
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add), //adciona um icone dentro do botão
        onPressed: () {
          setState(() {
            counter++;
          });
        },
      ),
    );
  }
}

//componentizando um widget
class CustomSwitcher extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Switch(
        value: AppController.instance.isDarkTheme,
        onChanged: (value) {
          AppController.instance.changeTheme();
        });
  }
}

//componentizando um widget
class Squares extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          width: 50,
          height: 50,
          color: Colors.black,
        ),
      ],
      mainAxisAlignment: MainAxisAlignment.spaceAround,
    );
  }
}

class SpaceVertical extends StatelessWidget {
  final double size;
  const SpaceVertical({Key? key, required this.size}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(height: size);
  }
}
