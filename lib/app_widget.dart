import 'package:conversor_curso/app_controller.dart';
import 'package:conversor_curso/home_page.dart';
import 'package:conversor_curso/login_page.dart';
import 'package:flutter/material.dart';
//import 'package:conversor_curso/desafio.dart';

class AppWidget extends StatelessWidget {
  final String title;

  const AppWidget({Key? Key, required this.title}) : super(key: Key);

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: AppController.instance,
      builder: (context, child) {
        return MaterialApp(
          theme: ThemeData(
            primarySwatch: Colors.amber,
            brightness: AppController.instance.isDarkTheme
                ? Brightness.dark
                : Brightness.light,
          ),
          //DEFINE QUE A ROTA INICIAL SERA A /
          initialRoute: '/',
          //DEFINE AS ROTAS QUE SERÃO USADAS
          routes: {
            '/': (context) => LoginPage(),
            '/home': (context) => HomePage(),
            //'/desafio': (context) => DesafioPage(),
          },
        );
      },
    );
  }
}
