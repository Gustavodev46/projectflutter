import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String email = '';
  String password = '';

  Widget _body() {
    return SingleChildScrollView(
      child: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        //COLOCA A COLUMN DENTRO DE UM PADDING PARA SE AFASTAR DA BORDA
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              //ADCIONA IMAGEM , CONTAINER PARA DEFINIR O TAMANHO
              Container(
                width: 100,
                height: 100,
                child: Image.asset('assets/images/logo.jpg'),
              ),
              Card(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      TextField(
                        //DEFINE QUE SEMPRE QUE O CAMPO FOR ALTERADO ACONTEÇA O QUE ESTIVER DENTRO DAS CHAVES(FUNÇÃO)
                        onChanged: (input) {
                          email = input;
                        },
                        //DEFINE QUE O CAMPO IRA RECEBER, FAZ ALGUMAS ALTERAÇÕES NO TECLADO DO CEL
                        keyboardType: TextInputType.emailAddress,
                        //DEFINE O QUE IRÁ DECORAR O TEXTFIELD
                        decoration: InputDecoration(
                          //DEFINE UM TITULO PARA A TEXTFIELD
                          labelText: 'Email',
                          //DEFINE A BORDA PARA A TEXTFIELD
                          border: OutlineInputBorder(),
                        ),
                      ),
                      //DEFINE UM ESPAÇO DE 10 ENTRE OS TEXTFIELD
                      SizedBox(height: 10),
                      TextField(
                        //DEFINE QUE SEMPRE QUE O CAMPO FOR ALTERADO ACONTEÇA O QUE ESTIVER DENTRO DAS CHAVES(FUNÇÃO)
                        onChanged: (input) {
                          password = input;
                        },
                        //DEFINE QUE AS PALAVRAS SERÃO OCULTAS AO DIGITAR (***)
                        obscureText: true,
                        //DEFINE O QUE IRÁ DECORAR O TEXTFIELD
                        decoration: InputDecoration(
                          //DEFINE UM TITULO PARA A TEXTFIELD
                          labelText: 'Password',
                          //DEFINE A BORDA PARA A TEXTFIELD
                          border: OutlineInputBorder(),
                        ),
                      ),
                      SizedBox(height: 15),
                      ElevatedButton(
                        onPressed: () {
                          if (email == 'gu@' && password == '123') {
                            //NAVEGA PARA A ROTA ESPECIFICADA
                            Navigator.of(context).pushReplacementNamed('/home');
                          } else {
                            print('LOGIN INVÁLIDO');
                          }
                        },
                        child: Text('Entrar'),
                      ),
                    ],
                  ),
                ),
              ),
              //DEFINE UM ESPAÇO DE 10 ENTRE OS TEXTFIELD E O BOTÃO
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //ADCIONA UM SCROL AOS SEUS CHILD
        body: Stack(
      children: [
        SizedBox(
          height: MediaQuery.of(context).size.height,
          child: Image.asset(
            'assets/images/background.jpg',
            fit: BoxFit.cover,
          ),
        ),
        Container(
          //OPACIDADE DO BACKGROUND
          color: Colors.black.withOpacity(0.0),
        ),
        _body(),
      ],
    ));
  }
}
